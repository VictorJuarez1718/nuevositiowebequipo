// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase,onValue,ref,set,get,child,update,remove } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { getAuth, signInWithEmailAndPassword, onAuthStateChanged, signOut} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyATkqZRDW7VqYN__TnU3bDhNo2Rd6Ch33I",
    authDomain: "sitioweb2023-7d9e9.firebaseapp.com",
    databaseURL: "https://sitioweb2023-7d9e9-default-rtdb.firebaseio.com",
    projectId: "sitioweb2023-7d9e9",
    storageBucket: "sitioweb2023-7d9e9.appspot.com",
    messagingSenderId: "452062774855",
    appId: "1:452062774855:web:9ed95091b5a50597dbd362"
  };
  

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();


// Variables generales a usar
var btnIngresar = document.getElementById('btnLogin');
var btnDesconectar = document.getElementById('btnDesconectar');
var btnLimpiar = document.getElementById('btnLimpiar');


var email = "";
var contraseña = "";


const auth = getAuth();


function comprobarDatos(){
    email = document.getElementById('correo').value;
    contraseña = document.getElementById('Contraseña').value;
}

/*
// Ayuda a comprobar si el usuario ya está conectado
function comprobarLogin(){
    onAuthStateChanged(auth, (user) => {
        if (user) {
          alert("Usuario detectado");
          
        } else {
          
          alert("No se ha detectado un usuario");
          window.location.href="principalSitioWeb.html";
        }
      });
}*/

// IF que comprueba el correo y contraseña
if(btnIngresar){
    btnIngresar.addEventListener('click', (e)=>{
        comprobarDatos();
        signInWithEmailAndPassword(auth, email, contraseña)
        .then((userCredential) => {
            const user = userCredential.user;
            alert("Ha iniciado sesión exitosamente")
            window.location.href = "pestañaAdmin.html";
        })
        .catch((error) => {
            alert("Los datos son incorrectos")
            const errorCode = error.code;
            const errorMessage = error.message;
            limpiar();
        });
    });
}

// IF que desconectará al usuario
if(btnDesconectar){
    btnDesconectar.addEventListener('click', (e)=>{
        signOut(auth).then(() => {
            alert("Desconexión exitosa")
            window.location.href = "principalSitioWeb.html";
        }).catch((error) => {
            
        })
    })
}


function limpiar(){
    email = document.getElementById('correo');
    contraseña = document.getElementById('Contraseña');
    email.value = "";
    contraseña.value = "";
    

    
}

btnLimpiar.addEventListener('click', limpiar)


